@extends('layouts.master')

@section('page-title') @parent
{{ $product->title }}
@stop

@section('content')

<div class="container">
	<h2>{{ $product->title }}</h2>
	<p>{{ $product->description }}</p>
	<p>${{ $product->price }}</p>
</div> <!-- /container -->

@stop